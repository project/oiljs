INTRODUCTION
------------

oil.js provides an integration with "Axel Springer Opt-In Layer". To learn more
about the Opt-In Layer, visit https://oil.axelspringer.com/.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/oiljs

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/oiljs


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

Go to admin/config/system/oiljs to configure the settings of your layer.
For a detailed list of strings and their defaults go to https://oil.axelspringer.com/docs/#full-label-configuration 

MAINTAINERS
-----------

Current maintainers:
 * Stefan Borchert (stborchert) - https://www.drupal.org/u/stborchert

This project has been sponsored by:
* undpaul
  Drupal experts providing professional Drupal development services.
  Visit https://www.undpaul.de for more information.
